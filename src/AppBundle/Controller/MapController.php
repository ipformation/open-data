<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MapController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @Template
     */
    public function indexAction(Request $request)
    {

        $poi = $this->getDoctrine()
            ->getRepository('AppBundle:Poi')
            ->findAll();

        $types = array();
        $points = array();
        $i =0;

        foreach ($poi as $point) {
            if (!in_array($point->getType(), $points)) {
                $types[$i] = $point->getType();
                $points[] = $point->getType();
            }
            $i++;
        }

        return array(
            'types' => $types
        );
    }

    /**
     * @Route("/grab-type", options={"expose"=true})
     */
    public function grabAction(Request $request)
    {
        $poiByType = array();

        if ($request->isXmlHttpRequest()) {

            $type = $request->request->get('type');

            //GRAB POI
            $poiByType = $this->getDoctrine()
                ->getRepository('AppBundle:Poi')
                ->findBy(array(
                    'type' => $type
                ));


        }
        $response = new Response();
        $response->setContent($this->container->get('serializer')->serialize($poiByType, 'json'));

        return $response;
    }
}
