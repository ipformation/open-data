
$( document ).ready(function() {

    if ($('#mapid').length) {

        var mymap = L.map('mapid').setView([51.505, -0.09], 13);

        var markers = [];

        L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
            attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery � <a href="http://mapbox.com">Mapbox</a>',
            maxZoom: 18,
            id: 'lrocher.pffbobi6',
            accessToken: 'pk.eyJ1IjoibHJvY2hlciIsImEiOiJjaW0yNXNscmkwMG44d2ZtNnoxa3EzYmtqIn0.qxa6k6WtvlsYTpXGpn2ALA'
        }).addTo(mymap);

        mymap.locate({setView: true, maxZoom: 15});
        mymap.on('locationfound', onLocationFound);

        function onLocationFound(e) {
            // create a marker at the users "latlng" and add it to the map
            L.circle(e.latlng, 100, {
                color: 'green',
                fillColor: 'green',
                fillOpacity: 0.2
            }).addTo(mymap);
        }

        function removeAllMarkers(){
            $.each(markers, function (k,marker) {
                mymap.removeLayer(marker);
            });
        }

        function pickupPoi(poi) {

            var args = {
                iconUrl: '',
                shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.3/images/marker-shadow.png',
                iconSize: [25, 41],
                iconAnchor: [12, 41],
                popupAnchor: [1, -34],
                shadowSize: [41, 41]
            };

            $.each(JSON.parse(poi), function (k,p) {
            switch (p.type) {
                case "RESTAURATION":
                    args.iconUrl = 'bundles/app/img/marker-icon-black.png';
                    break;
                case "COMMERCE_ET_SERVICE":
                    args.iconUrl = 'bundles/app/img/marker-icon-blue.png';
                    break;
                case "HEBERGEMENT_LOCATIF":
                    args.iconUrl = 'bundles/app/img/marker-icon-green.png';
                    break;
                case "PATRIMOINE_CULTUREL":
                    args.iconUrl = 'bundles/app/img/marker-icon-grey.png';
                    break;
                case "HEBERGEMENT_COLLECTIF":
                    args.iconUrl = 'bundles/app/img/marker-icon-green.png';
                    break;
                case "EQUIPEMENT":
                    args.iconUrl = 'bundles/app/img/marker-icon-red.png';
                    break;
                case "HOTELLERIE":
                    args.iconUrl = 'bundles/app/img/marker-icon-orange.png';
                    break;
                case "DEGUSTATION":
                    args.iconUrl = 'bundles/app/img/marker-icon-yellow.png';
                    break;
                case "HOTELLERIE_PLEIN_AIR":
                    args.iconUrl = 'bundles/app/img/marker-icon-orange.png';
                    break;
                case "PATRIMOINE_NATUREL":
                    args.iconUrl = 'bundles/app/img/marker-icon-grey.png';
                    break;
                }
                
                var icon = new L.Icon(args);
                
                var marker = L.marker([p.longitude, p.latitude], {icon: icon}).addTo(mymap)
                    .bindPopup("Type de pnt d'interet : " + p.type + "<br/>" + p.name + "<br/>" + p.adress + " " + p.postal_code + " " + p.city)
                    //.openPopup();


                markers.push(marker);

            });

            return false;

        }

        $(document).on('click','.mm-menu__item a',function(){
            var type = $(this).data('type');
            $('.mm-menu-toggle').removeClass('active');
            $('.mm-menu').removeClass('active');
            $('.mm-menu-mask').removeClass('active');
            $('.loader').removeClass('hidden');
            $(this).addClass('active');

            $.ajax({
                type     : "POST",
                cache    : false,
                url      : GLOBALS.urls.grabPoiByType,
                data     : {'type': type},
                success: function(data) {
                    removeAllMarkers();
                    pickupPoi(data);
                    $('.loader').delay(4500).addClass('hidden');

                },
                error: function () {
                    console.log('error');
                }
            });
        });




    }

    $(document).on('click','#mm-menu li.admin a',function(){
        var type = $(this).data('type');
        $('.mm-menu-toggle').removeClass('active');
        $('.mm-menu').removeClass('active');
        $('.mm-menu-mask').removeClass('active');

        $.ajax({
            type     : "POST",
            cache    : false,
            url      : GLOBALS.urls.grabAdminPoiByType,
            data     : {'type': type},
            success: function(data) {
                $('#wrapper').empty();
                $('#wrapper').append(data);
            }
        });
    });

    $(document).on('click','button#grab',function(){
        console.log('ok');
        $.ajax({
            type     : "POST",
            cache    : false,
            url      : GLOBALS.urls.grabPoi,
            success: function(data) {
                if (data.success == false) {
                    alert('Succès')
                }
                else {
                    alert('Pas succès')
                }
            }
        });
    });

    var menu = new Menu;

});
