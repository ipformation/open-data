<?php

namespace UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class PoiType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('detail', null, array(
                'attr'=>
                    array(
                        'class'=>'mdl-textfield__input'
                    ),
                'label_attr' => array(
                'class' => 'mdl-textfield__label'
                )
            ))
            ->add('website', null, array(
                'attr'=>
                    array(
                        'class'=>'mdl-textfield__input'
                    ),
                'label_attr' => array(
                    'class' => 'mdl-textfield__label'
                )
            ))
            ->add('city', null, array(
                'attr'=>
                    array(
                        'class'=>'mdl-textfield__input'
                    ),
                'label_attr' => array(
                    'class' => 'mdl-textfield__label'
                )
            ))
            ->add('adress', null, array(
                'attr'=>
                    array(
                        'class'=>'mdl-textfield__input'
                    ),
                'label_attr' => array(
                    'class' => 'mdl-textfield__label'
                )
            ))
            ->add('phone', null, array(
                'attr'=>
                    array(
                        'class'=>'mdl-textfield__input'
                    ),
                'label_attr' => array(
                    'class' => 'mdl-textfield__label'
                )
            ))
            ->add('postalCode', null, array(
                'attr'=>
                    array(
                        'class'=>'mdl-textfield__input'
                    ),
                'label_attr' => array(
                    'class' => 'mdl-textfield__label'
                )
            ))
            ->add('save', SubmitType::class, array(
                'label' => 'Edit',
                'attr' =>
                    array(
                        'class' => 'mdl-button mdl-js-button mdl-button--raised'
                    ),
            ))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Poi'
        ));
    }
}
