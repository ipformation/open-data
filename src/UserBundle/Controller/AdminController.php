<?php

namespace UserBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Entity\Poi;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AdminController
 * @package UserBundle\Controller
 * @Route("/admin")
 */
class AdminController extends Controller
{

    /**
     * @Route("/dashboard")
     * @Template
     */
    public function indexAction()
    {

        $poi = $this->getDoctrine()
            ->getRepository('AppBundle:Poi')
            ->findAll();

        $types = array();
        $points = array();
        $i =0;

        foreach ($poi as $point) {
            if (!in_array($point->getType(), $points)) {
                $types[$i] = $point->getType();
                $points[] = $point->getType();
            }
            $i++;
        }

        $poiByType = $this->getDoctrine()
            ->getRepository('AppBundle:Poi')
            ->findBy(array(
                'type' => $types[0]
            ));

        return array(
            'types' => $types,
            'poi' => $poiByType
        );
    }

    /**
     * @Route("/edit/{id}")
     * @Template
     * @param Request $request
     * @param $id
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, $id)
    {

        $poi = $this->getDoctrine()
            ->getRepository('AppBundle:Poi')
            ->findOneBy(array(
                'id' => $id
            ));

        $editForm = $this->createForm('UserBundle\Form\PoiType', $poi);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($poi);
            $em->flush();
        }

        return array(
            'poiForm' => $editForm->createView()
        );
    }

    /**
     * @Route("/grab", options={"expose"=true})
     */
    public function grabAction(Request $request)
    {
        if ($request->isXmlHttpRequest()) {

            $data = json_decode(file_get_contents('https://download.data.grandlyon.com/wfs/rdata?SERVICE=WFS&VERSION=2.0.0&outputformat=GEOJSON&request=GetFeature&typename=sit_sitra.sittourisme&SRSNAME=urn:ogc:def:crs:EPSG::4171', true));

            $batchSize = 5;
            $em = $this->getDoctrine()->getManager();

            foreach ($data->features as $i => $item) {
                $poi = new Poi();

                $em->persist($poi);

                $poi->setName($item->properties->nom);
                $poi->setAdress($item->properties->adresse);
                $poi->setType($item->properties->type);
                $poi->setDetail($item->properties->type_detail);
                $poi->setCity($item->properties->commune);
                $poi->setPostalCode($item->properties->codepostal);
                $poi->setLatitude($item->geometry->coordinates[0]);
                $poi->setLongitude($item->geometry->coordinates[1]);

                // flush everything to the database every 20 inserts
                if (($i % $batchSize) == 0) {
                    $em->flush();
                    $em->clear();
                }
            }

            //flush the remaining objects
            $em->flush();
            $em->clear();

            $this->addFlash(
                'notice',
                'Your changes were saved!'
            );

        }

    }

    /**
     * @Route("/grab-by-type", options={"expose"=true})
     * @Template
     */
    public function grabPoiByTypeAction(Request $request)
    {

        if ($request->isXmlHttpRequest()) {
            $type = $request->request->get('type');

            //GRAB POI
            $poiByType = $this->getDoctrine()
                ->getRepository('AppBundle:Poi')
                ->findBy(array(
                    'type' => $type
                ));
        }

        return array(
            'poi' => $poiByType
        );

    }
}
